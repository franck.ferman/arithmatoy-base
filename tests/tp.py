def nombre_entier(n: int) -> str:
    return "0" if n == 0 else "S" + nombre_entier(n - 1)

def S(n: str) -> str:
    return "S" + n

def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    elif b == "0":
        return a
    else:
        return S(addition(a[:-1], b[:-1]))

def multiplication(a: str, b: str) -> str:
    if a == "0" or b == "0":
        return "0"
    else:
        return addition(b, multiplication(a[:-1], b))

def facto_ite(n: int) -> int:
    if n < 0:
        raise ValueError("Negative integer.")
    result = 1
    for i in range(1, n+1):
        result *= i
    return result

def facto_rec(n: int) -> int:
    if n < 0:
        raise ValueError("Negative integer.")
    return 1 if n == 0 else n * facto_rec(n-1)

def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)

def fibo_ite(n: int) -> int:
    a, b = 0, 1
    for i in range(n):
        a, b = b, a+b
    return a

def golden_phi(n: int) -> float:
    phi = (1 + math.sqrt(5)) / 2
    return (phi ** n - (1 - phi) ** n) / math.sqrt(5)

def sqrt5(n):
    phi = golden_phi(n+1)
    return 2 * phi - 1

def pow(x: float, n: int) -> float:
    if n == 0:
        return 1
    elif n % 2 == 0:
        return pow(x * x, n // 2)
    else:
        return x * pow(x, n-1)

